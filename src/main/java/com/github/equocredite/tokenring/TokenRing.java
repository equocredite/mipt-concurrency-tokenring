package com.github.equocredite.tokenring;

import com.github.equocredite.tokenring.edge.Edge;
import static com.github.equocredite.tokenring.Util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;


public class TokenRing {
    private final List<Node> nodes = new ArrayList<>();
    private final List<Edge> edges = new ArrayList<>();
    private final List<Thread> threads = new ArrayList<>();
    private final TokenInitializer tokenInitializer;
    private long startTime;
    private Long stopTime;
    private int nTokens;

    public TokenRing(int nNodes, Supplier<Edge> edgeSupplier, TokenInitializer tokenInitializer) {
        this.tokenInitializer = tokenInitializer;
        for (int i = 0; i < nNodes; ++i) {
            edges.add(edgeSupplier.get());
        }
        for (int i = 0; i < nNodes; ++i) {
            nodes.add(new Node(i, edges.get(i), edges.get((i + 1) % nNodes)));
        }
    }

    public void initTokens() throws InterruptedException {
        nTokens = tokenInitializer.init(edges);
    }

    public void start() throws InterruptedException {
        for (var node : nodes) {
            threads.add(new Thread(node::run));
        }
        for (var thread : threads) {
            thread.start();
        }
        startTime = System.nanoTime();
        initTokens();
    }

    public void stop() {
        for (var thread : threads) {
            thread.interrupt();
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException ignored) {
            }
        }
        stopTime = System.nanoTime();
    }

    public Stats calcStats() {
        if (!isStopped()) {
            throw new IllegalStateException("Network hasn't stopped yet");
        }
        double duration = getDurationSeconds();
        double throughput = 0;
        double latency = 0;
        for (var node : nodes) {
            throughput += node.getNTokensProcessed() / duration / nodes.size();
        }
        for (var edge : edges) {
            while (!edge.isEmpty()) {
                Token token = null;
                try {
                    token = edge.recv();
                } catch (InterruptedException ignored) {
                }
                assert token != null;
                latency += 1. * (token.getLastRoundEndTime() - token.getStartTime())
                        / token.getNRounds()
                        / edges.size()
                        / nTokens;
            }
        }
        return new Stats(throughput, latency);
    }

    private boolean isStopped() {
        return stopTime != null;
    }

    private double getDurationSeconds() {
        if (!isStopped()) {
            throw new IllegalStateException("Network hasn't stopped yet");
        }
        return secondsBetweenNano(startTime, stopTime);
    }
}
