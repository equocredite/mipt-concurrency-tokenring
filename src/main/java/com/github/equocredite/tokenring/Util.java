package com.github.equocredite.tokenring;

public class Util {
    public static double secondsBetweenNano(long left, long right) {
        return (right - left) / 1_000_000_000.;
    }
}
