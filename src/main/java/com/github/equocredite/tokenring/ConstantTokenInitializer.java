package com.github.equocredite.tokenring;

import com.github.equocredite.tokenring.edge.Edge;

import java.util.List;

public class ConstantTokenInitializer implements TokenInitializer {
    private final int tokensPerEdge;

    public ConstantTokenInitializer(int tokensPerEdge) {
        this.tokensPerEdge = tokensPerEdge;
    }

    @Override
    public int init(List<Edge> edges) {
        int n = edges.size();
        for (var edge : edges) {
            for (int i = 0; i < tokensPerEdge; ++i) {
                try {
                    edge.send(new Token((i - 1 + n) % n));
                } catch (InterruptedException ignored) {
                }
            }
        }
        return n * tokensPerEdge;
    }
}
