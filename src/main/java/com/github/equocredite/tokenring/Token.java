package com.github.equocredite.tokenring;

public class Token {
    private final int startNodeId;
    private final long startTime;
    private long lastRoundEndTime;
    private long nRounds;

    public Token(int startNodeId) {
        this.startNodeId = startNodeId;
        startTime = System.nanoTime();
    }

    public void completeRound() {
        lastRoundEndTime = System.nanoTime();
        ++nRounds;
    }

    public int getStartNodeId() {
        return startNodeId;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getLastRoundEndTime() {
        return lastRoundEndTime;
    }

    public long getNRounds() {
        return nRounds;
    }
}
