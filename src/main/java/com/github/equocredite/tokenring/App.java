package com.github.equocredite.tokenring;

import com.github.equocredite.tokenring.edge.ArrayBlockingQueueEdge;
import com.github.equocredite.tokenring.edge.ConcurrentLinkedQueueEdge;

public class App {
    public static void main(String[] args) {
        int nNodes = args.length > 1 ? Integer.parseInt(args[1]) : Runtime.getRuntime().availableProcessors();
        System.out.println("nNodes = " + nNodes);
        for (int i = 1; i <= 40; ++i) {
            var ring = new TokenRing(
                    nNodes,
                    () -> new ConcurrentLinkedQueueEdge(),
                    new ConstantTokenInitializer(i * 5)
            );
            try {
                ring.start();
                Thread.sleep(5000);
            } catch (InterruptedException ignored) {
            }
            ring.stop();
            var stats = ring.calcStats();
            System.out.println(i * 5. / nNodes + " " + (int)stats.getThroughput() + " " + (int)stats.getLatency());
        }
    }
}
