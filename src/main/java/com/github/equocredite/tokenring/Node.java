package com.github.equocredite.tokenring;

import com.github.equocredite.tokenring.edge.Edge;

public class Node {
    private final int id;
    private final Edge in;
    private final Edge out;
    private long nTokensReceived = 0;


    public Node(int id, Edge in, Edge out) {
        this.id = id;
        this.in = in;
        this.out = out;
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                var token = in.recv();
                if (token == null) {
                    return;
                }
                process(token);
                out.send(token);
            } catch (InterruptedException ex) {
                return;
            }
        }
    }

    public long getNTokensProcessed() {
        return nTokensReceived;
    }

    private void process(Token token) {
        ++nTokensReceived;
        if (token.getStartNodeId() == id) {
            token.completeRound();
        }
    }
}
