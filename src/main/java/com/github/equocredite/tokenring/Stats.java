package com.github.equocredite.tokenring;

public class Stats {
    private final double throughput;
    private final double latency;

    public Stats(double throughput, double latency) {
        this.throughput = throughput;
        this.latency = latency;
    }

    public double getThroughput() {
        return throughput;
    }

    public double getLatency() {
        return latency;
    }
}
