package com.github.equocredite.tokenring;

import com.github.equocredite.tokenring.edge.Edge;

import java.util.List;

public interface TokenInitializer {
    int init(List<Edge> edges) throws InterruptedException;
}
