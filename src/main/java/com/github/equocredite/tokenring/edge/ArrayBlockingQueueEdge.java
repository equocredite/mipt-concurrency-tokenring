package com.github.equocredite.tokenring.edge;

import com.github.equocredite.tokenring.Token;

import java.util.concurrent.ArrayBlockingQueue;

public class ArrayBlockingQueueEdge implements Edge {
    private final ArrayBlockingQueue<Token> queue;

    public ArrayBlockingQueueEdge(int capacity) {
        queue = new ArrayBlockingQueue<>(capacity);
    }

    @Override
    public void send(Token token) throws InterruptedException {
        queue.put(token);
    }

    @Override
    public Token recv() throws InterruptedException {
        return queue.take();
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
