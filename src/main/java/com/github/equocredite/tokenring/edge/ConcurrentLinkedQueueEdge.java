package com.github.equocredite.tokenring.edge;

import com.github.equocredite.tokenring.Token;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ConcurrentLinkedQueueEdge implements Edge {
    private final ConcurrentLinkedQueue<Token> queue = new ConcurrentLinkedQueue<>();

    @Override
    public void send(Token token) {
        queue.add(token);
    }

    @Override
    public Token recv() {
        Token token;
        do {
            token = queue.poll();
        } while (token == null && !Thread.currentThread().isInterrupted());
        return token;
    }

    @Override
    public boolean isEmpty() {
        return queue.isEmpty();
    }
}
