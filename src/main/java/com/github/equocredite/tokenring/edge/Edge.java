package com.github.equocredite.tokenring.edge;

import com.github.equocredite.tokenring.Token;

public interface Edge {
    void send(Token token) throws InterruptedException;

    Token recv() throws InterruptedException;

    boolean isEmpty();
}
